//
//  JSONParsing.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 21.09.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

//паттерн интерпретатор - предващение json в словарь
import UIKit

class JSONParsing {

    static func getDictionaryFromJSON(nodeName: String, data: Data?)-> [String:Any]{
        do{
            let readableJSON = try JSONSerialization.jsonObject(with: data!, options:.mutableContainers) as! [String:Any]
            if (readableJSON["error"] != nil) {
                print ("error : \(String(describing: readableJSON["error"]))")
                return [:]
            }
            return readableJSON[nodeName] as! [String:Any]
        }
        catch {
            print(error)
        }
        return [:]
    }
    
    static func getArrayFromJSON(nodeName: String, data: Data?)-> NSArray{
        do{
            let readableJSON = try JSONSerialization.jsonObject(with: data!, options:.mutableContainers) as! [String:Any]
            if (readableJSON["error"] != nil) {
                print ("error : \(String(describing: readableJSON["error"]))")
                return []
            }
            return readableJSON[nodeName] as! NSArray
        }
        catch {
            print(error)
        }
        return []
    }
}
