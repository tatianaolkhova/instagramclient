//
//  Constants.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 21.09.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

public class Constants {

    /// Страницы авторизации
    public static var authUrl: URL {
        get{
            let stringUrl = "https://api.instagram.com/oauth/authorize/?client_id=b77bf1e941ce42e4aa6ceb0cd94eb952&scope=public_content+follower_list+relationships+comments+likes&redirect_uri=https://www.instagram.com&response_type=token"
            return URL(string: stringUrl)!
        }
    }
    
}
