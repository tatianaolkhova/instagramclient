//
//  AppOperationQueue.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 27.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

class AppOperationQueue {

    public static let sharedInstance: AppOperationQueue = AppOperationQueue()
    
    public let queue: OperationQueue = {
        let queue = OperationQueue()
        queue.qualityOfService = .userInteractive
        return queue
    }()

}
