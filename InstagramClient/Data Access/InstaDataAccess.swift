//
//  InstaDataAccess.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 21.09.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class InstaDataAccess {

    //общий запрос к серверу Insta
    static func _request(method: String, resultPerform: @escaping (DataResponse<Any>) -> Void)
    {
        DispatchQueue.global().async {
            Alamofire.request (method).responseJSON(queue: DispatchQueue.global(), options: .mutableContainers, completionHandler: {
                response in
                resultPerform(response)
            })
        }
    }
    
    //Alt+Cmd+/
    /// <#Description#>
    ///
    /// - Parameter updateFunc: <#updateFunc description#>
    static func GetUserDetails(updateFunc: @escaping(_ user: User) -> Void) {
        
        _request(method: "https://api.instagram.com/v1/users/self/?access_token=" + Credential.sharedInstance.token!, resultPerform: {
            response in
            let user_info = JSONParsing.getDictionaryFromJSON(nodeName: "data", data: response.data)
            print("\(user_info)")
            let user: User = User(data: user_info)  //User(data: user_info)
            DispatchQueue.main.async {
                updateFunc(user)
            }
        })
    }
    
    //https://api.instagram.com/v1/tags/nofilter/media/recent?access_token=ACCESS_TOKEN
    static func GetMediaRecent(updateFunc: @escaping(_ user: [Media]) -> Void) {
        
        _request(method: "https://api.instagram.com/v1/users/self/media/recent?access_token=" + Credential.sharedInstance.token! + "&count=20", resultPerform: {
            response in
            let medias = JSONParsing.getArrayFromJSON(nodeName: "data", data: response.data)

            var mediasArray : Array<Media> = []
            
            for e in (medias ) {
                mediasArray.append(Media(data: e as! [String:Any]))
            }
            DispatchQueue.main.async {
                updateFunc(mediasArray)
            }
        })
    }
    
}
