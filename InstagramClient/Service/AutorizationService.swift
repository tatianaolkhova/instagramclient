//
//  AutorizationService.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 20.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

class AutorizationService {

    private let _login: AutorizationCommand
    private let _logout: AutorizationCommand
    
    public init() {
        self._login = LoginCommand()
        self._logout = LogoutCommand()
    }
    
    public func login() {
        self._login.execute()
    }
    
    public func logout() {
        self._logout.execute()
    }
}
