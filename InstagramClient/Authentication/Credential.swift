//
//  Credential.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 21.09.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

//паттерн хранитель
class Credential {
    
    public static let sharedInstance: Credential = Credential()
    public static let tokenKey = "Credential.tokenKey"
    
    
    /// Показывает зарегистрирован ли пользователь на текущий момент.
    public var isAuthorized: Bool {
        get{
            guard let _ = self.token else {
                return false
            }
            if (self.token == "") {
                return false
            }
            return true
        }
    }
    
    /// Хранит ссесионный ключ пользователя.
    /// (возвращает nil в случае его отсутствия)
    public var token: String? {
        get {
            return UserDefaults.standard.value(forKey: Credential.tokenKey) as? String
        }
        set{
            UserDefaults.standard.set(newValue, forKey: Credential.tokenKey)
        }
    }
    
    public func clearData() {
        token = "";
    }
}
