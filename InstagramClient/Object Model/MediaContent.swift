//
//  Content.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 06.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

//паттерн компановщик
import UIKit

protocol MediaContent {

    var width: Int64 {get}
    var height: Int64 {get}
    var url: String {get}
    
    func show()
    
    //var type = <#value#>
    
}
