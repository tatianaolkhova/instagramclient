//
//  InitFromDictionary.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 22.09.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

protocol InitFromDictionary {
    init(data: [String:Any])
}
