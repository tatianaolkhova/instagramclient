//
//  Photo.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 22.09.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

class Image : InitFromDictionary, MediaContent {
    func show() {
    }
    
    func show(updateFunc: @escaping(_ outputImage: UIImage) -> Void)
    {
        let getCacheImage = GetCacheImage(url: url)
        getCacheImage.completionBlock = {
            OperationQueue.main.addOperation {
                updateFunc(getCacheImage.outputImage!)
            }
        }
        AppOperationQueue.sharedInstance.queue.addOperation(getCacheImage)
    }
    
    required init(data: [String : Any]) {
        width = data["width"] as! Int64
        height = data["height"] as! Int64
        url = data["url"] as! String
    }
    
    var width: Int64
    var height: Int64
    var url: String
}
