//
//  User.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 21.09.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

struct User: InitFromDictionary {
    init(data: [String : Any]) {
        name = data["full_name"] as! String
        email =  ""
        phone = ""
        id = data["id"] as! String
        profile_picture = data["profile_picture"] as! String
        username = data["username"] as! String
    }
    
    let name: String
    let email: String
    let phone: String
    let id: String
    let profile_picture: String
    let username: String
}

