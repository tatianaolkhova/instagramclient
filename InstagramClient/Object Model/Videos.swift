//
//  Videos.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 27.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

class Videos: InitFromDictionary, MediaUrl {
    var standardContentUrl: String
    
    func showOnController(controller: ControllerToShowWebContent) {
        controller.setUrl(mediaUrl: self)
    }
    
    
    required init(data: [String : Any]) {
        low_resolution = Video(data: data["low_resolution"] as! [String:Any])
        standard_resolution = Video(data: data["standard_resolution"] as! [String:Any])
        low_bandwidth = Video(data: data["low_bandwidth"] as! [String:Any])
        
        standardContentUrl = low_resolution.url
    }
    
    init() {
        low_resolution = Video()
        standard_resolution = Video()
        low_bandwidth = Video()
        
        standardContentUrl = ""
    }
    
    var low_resolution: Video
    var standard_resolution: Video
    var low_bandwidth: Video

}
