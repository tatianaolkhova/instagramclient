//
//  Media.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 27.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

enum MediaType {
    case image
    case video
}

class Media {
    
    required init(data: [String : Any]) {
        id = data["id"] as! String
        created_time = Date(timeIntervalSince1970: Double(data["created_time"] as! String)!)
        
        let caption = data["caption"] as? [String:Any]
        if (caption != nil){
            caption_text = caption!["text"] as! String
        }
        else {caption_text = ""}
        
        link = data["link"] as! String
        type = ((data["type"] as! String) == "video") ? MediaType.video : MediaType.image
        if (type == MediaType.video){
            videos = Videos(data: data["videos"] as! [String: Any])
        }
        else {videos = Videos()}
        images = Images(data: data["images"] as! [String: Any])
        author = User(data: data["user"] as! [String: Any])
    }
    
    var id: String
    var created_time: Date
    var caption_text: String
    var link: String
    var type: MediaType
    var author: User
    var images: Images
    var videos: Videos

    func getMediaUrl() -> MediaUrl {
        return type == MediaType.image ? images : videos
    }
}
