//
//  Video.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 06.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

class Video: InitFromDictionary, MediaContent {
    func show() {
        //smth
    }
    
    required init(data: [String : Any]) {
        id = data["id"] as! String
        width = data["width"] as! Int64
        height = data["height"] as! Int64
        url = data["url"] as! String
    }
    
    init() {
        id = ""
        width = 0
        height = 0
        url = ""
    }
    
    var id: String
    var width: Int64
    var height: Int64
    var url: String
}
