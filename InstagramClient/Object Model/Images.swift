//
//  Images.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 27.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

class Images: InitFromDictionary, MediaUrl {
    var standardContentUrl: String
    
    func showOnController(controller: ControllerToShowWebContent) {
        controller.setUrl(mediaUrl: self)
    }
    

    required init(data: [String : Any]) {
        low_resolution = Image(data: data["low_resolution"] as! [String:Any]);
        standard_resolution = Image(data: data["standard_resolution"] as! [String:Any]);
        thumbnail = Image(data: data["thumbnail"] as! [String:Any]);
        
        standardContentUrl = low_resolution.url
    }
    
    var low_resolution: Image
    var standard_resolution: Image
    var thumbnail: Image
}
