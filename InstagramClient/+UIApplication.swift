//
//  +UIApplication.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 20.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

extension UIApplication {

    static var topViewController: UIViewController? {
        if var viewController: UIViewController = UIApplication.shared.keyWindow?.rootViewController {
            while viewController.presentedViewController != nil {
                viewController = viewController.presentedViewController!
            }
            return viewController
        }
        return nil;
    }
}
