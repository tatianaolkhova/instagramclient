//
//  LoginCommand.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 20.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

class LoginCommand: AutorizationCommand {

    func execute() {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainNavigationVC")
        UIApplication.topViewController?.present(viewController, animated: true)
    }
}
