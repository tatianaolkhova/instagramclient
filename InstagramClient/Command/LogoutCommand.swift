//
//  LogoutCommand.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 20.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

class LogoutCommand: AutorizationCommand {

    func execute() {
        Credential.sharedInstance.clearData()
        let viewController = AuthorizationViewController()
        UIApplication.topViewController?.present(viewController, animated: true)
    }
}
