//
//  AutorizationCommand.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 20.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

protocol AutorizationCommand {
    func execute()
}
