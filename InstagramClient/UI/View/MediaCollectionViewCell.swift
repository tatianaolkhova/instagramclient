//
//  MediaCollectionViewCell.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 27.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

class MediaCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var image: UIImageView!
    
}
