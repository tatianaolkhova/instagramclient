//
//  AuthorizationViewController.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 21.09.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit
import SnapKit

class AuthorizationViewController: UIViewController, UIWebViewDelegate {

    private weak var _webView: UIWebView?
    private weak var _loadIndicator: UIActivityIndicatorView?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self._addComponets()
        self._auth()
    }
    
    private func _addComponets() {
        self.view.backgroundColor = UIColor.white
        
        let webViewTopOffset = 42
        let webView = UIWebView()
        webView.delegate = self
        self.view.addSubview(webView)
        webView.snp.makeConstraints { (make) -> Void in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo(self.view).offset(webViewTopOffset)
        }
        self._webView = webView
        
        let loadIndicatorSize = 64
        let loadIndicator = UIActivityIndicatorView()
        loadIndicator.tintColor = UIColor.blue
        self.view.addSubview(loadIndicator)
        loadIndicator.snp.makeConstraints { (make) -> Void in
            make.center.equalTo(self.view)
            make.width.height.equalTo(loadIndicatorSize)
        }
        _loadIndicator = loadIndicator
        loadIndicator.startAnimating()
    }
    
    private func _auth() {
        let request = URLRequest(url: Constants.authUrl)
        self._webView?.loadRequest(request)
    }
    
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        guard let url = request.url?.absoluteString else {
            return true
        }
        if url.range(of: "access_token") != nil {
            let accessToken = url.components(separatedBy: "#access_token=").last!
            Credential.sharedInstance.token = accessToken
            //self._presentNextScreen()
            AutorizationService().login()
        }
        
        return true
    }
    
    public func webViewDidStartLoad(_ webView: UIWebView) {
        self._loadIndicator?.isHidden = true
        self._loadIndicator?.stopAnimating()
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        self._loadIndicator?.isHidden = false
        self._loadIndicator?.startAnimating()
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        /// Обработка ошибок
    }
    
    /*
    private func _presentNextScreen() {
        let viewController = ViewController()
        self.present(viewController,
                     animated: true, completion: nil)
    }*/

}
