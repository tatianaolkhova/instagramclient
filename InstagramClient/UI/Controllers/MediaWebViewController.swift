//
//  MediaWebViewController.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 27.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit
import WebKit

class MediaWebViewController: UIViewController, WKNavigationDelegate, ControllerToShowWebContent {
    
    func setUrl(mediaUrl: MediaUrl) {
        curUrl = mediaUrl.standardContentUrl
    }
    
    @IBOutlet weak var webView: WKWebView!
    var curUrl: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView.navigationDelegate = self
        if let url = URL(string: curUrl) {
            webView.load(URLRequest(url: url))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished navigating to url \(webView.url)")
        
    }
}
