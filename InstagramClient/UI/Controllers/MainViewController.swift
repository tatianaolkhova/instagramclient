//
//  MainViewController.swift
//  InstagramClient
//
//  Created by Tatyana Olhova on 20.10.2018.
//  Copyright © 2018 Tatyana Olkhova. All rights reserved.
//

import UIKit

class MainViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource{

    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userPicture: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    var medias: [Media] = []
    
    //создаем объект очереди - пока только для кэширования изображений
    /*let queue: OperationQueue = {
        let queue = OperationQueue()
        queue.qualityOfService = .userInteractive
        return queue
    }()*/
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.fullNameLabel.text = ""
        self.idLabel.text = ""
        self.usernameLabel.text = ""
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        InstaDataAccess.GetUserDetails(updateFunc: {user in
            
            self.fullNameLabel.text = user.name
            self.idLabel.text = user.id
            self.usernameLabel.text = user.username
            let imageUrl:URL = URL(string: user.profile_picture)!
            let imageData:NSData = NSData(contentsOf: imageUrl)!
            self.userPicture.image = UIImage(data: imageData as Data)
        })
        
        InstaDataAccess.GetMediaRecent(updateFunc: {medias in
            self.medias = medias
            self.collectionView.reloadData()
        })
    }

    @IBAction func logout(_ sender: Any) {
        AutorizationService().logout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return medias.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaCell", for: indexPath) as! MediaCollectionViewCell
        
        medias[indexPath.row].images.thumbnail.show(updateFunc: {outputImage in
            cell.image.image = outputImage
        })
        
        /*
        //картинки сохраняем в кэш и берем оттуда, если там они уже есть
        let getCacheImage = GetCacheImage(url: (medias[indexPath.row].images.thumbnail.url))
        getCacheImage.completionBlock = {
            OperationQueue.main.addOperation {
                cell.image.image = getCacheImage.outputImage
            }
        }
        AppOperationQueue.sharedInstance.queue.addOperation(getCacheImage) */

        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destinationController = segue.destination as! MediaWebViewController
        medias[collectionView.indexPathsForSelectedItems![0].row].getMediaUrl().showOnController(controller: destinationController)
    }
    
}
